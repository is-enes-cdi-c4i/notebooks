# C4I Use Cases as Jupyter Notebooks

***A collection of Jupyter Notebooks implementing some Use Cases using [icclim](https://github.com/cerfacs-globc/icclim).***

Please note that all notebooks are only compatible with icclim v5.x and higher. 

The current version of icclim is [icclim 7.0.0](https://github.com/cerfacs-globc/icclim).

The [documentation](https://icclim.readthedocs.io/en/latest/) is also available.

icclim 7.0.0 can be installed using pip: `pip install icclim ` or conda forge: `conda install -c conda-forge icclim`

The notebooks in this repository can be executed in a [Climate4Impact v2](https://www.climate4impact.eu/). Please [Register](https://www.climate4impact.eu/c4i-frontend/register) to C4I to do so!

Follow the descriptions in each notebook to identify the datasets used for the analysis and sarch them in C4I.


